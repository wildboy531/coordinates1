﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coordinates_Dialog
{
    public class Coordinates : INotifyPropertyChanged
    {
        private int latitudeDegree = 20;
        private int latitudeMinute = 10;
        private int latitudeSecond = 10;
        private int longitudeDegree = 20;
        private int longitudeMinute = 30;
        private int longitudeSecond = 10;
        private int latitudeSecondDM = 12;
        private int longitudeSecondDM = 12;
        private double latitude = 11;
        private double longitude = 11;

        public Coordinates()
        {
            LatitudeDegree = 30;
        }

        public Coordinates(int latitudeDegree, int latitudeMinute, int latitudeSecond, int longitudeDegree, int longitudeMinute, int longitudeSecond, int latitudeSecondDM, int longitudeSecondDM, double latitude, double longitude)
        {
            this.latitudeDegree = latitudeDegree;
            this.latitudeMinute = latitudeMinute;
            this.latitudeSecond = latitudeSecond;
            this.longitudeDegree = longitudeDegree;
            this.longitudeMinute = longitudeMinute;
            this.longitudeSecond = longitudeSecond;
            this.latitudeSecondDM = latitudeSecondDM;
            this.longitudeSecondDM = longitudeSecondDM;
            this.latitude = latitude;
            this.longitude = longitude;
        }

        public int LatitudeDegree { get => latitudeDegree; set { latitudeDegree = value; OnPropertyChanged("LatitudeDegree"); } }
        public int LatitudeMinute { get => latitudeMinute; set { latitudeMinute = value; OnPropertyChanged("LatitudeMinute"); } }
        public int LatitudeSecond { get => latitudeSecond; set { latitudeSecond = value; OnPropertyChanged("LatitudeSecond"); } }
        public int LongitudeDegree { get => longitudeDegree; set { longitudeDegree = value; OnPropertyChanged("LongitudeDegree"); } }
        public int LongitudeMinute { get => longitudeMinute; set { longitudeMinute = value; OnPropertyChanged("LongitudeMinute"); } }
        public int LongitudeSecond { get => longitudeSecond; set { longitudeSecond = value; OnPropertyChanged("LongitudeSecond"); } }
        public int LatitudeSecondDM { get => latitudeSecondDM; set { latitudeSecondDM = value; OnPropertyChanged("LatitudeSecondDM"); } }
        public int LongitudeSecondDM { get => longitudeSecondDM; set { longitudeSecondDM = value; OnPropertyChanged("LongitudeSecondDM"); } }
        public double Latitude { get => latitude; set { latitude = value; OnPropertyChanged("Latitude"); } }
        public double Longitude { get => longitude; set { longitude = value; OnPropertyChanged("Longitude"); } }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public void SecondDMChanged()
        {
            Validate(LatitudeSecondDM, 99, out latitudeSecondDM);
            Validate(LongitudeSecondDM, 99, out longitudeSecondDM);
            LatitudeSecond = LatitudeSecondDM / 10 * 6;
            LongitudeSecond = LongitudeSecondDM / 10 * 6;
            Latitude = LatitudeDegree + (LatitudeMinute / 60) + (LatitudeSecondDM * 6 / 10 / 3600);
            Longitude = LongitudeDegree + (LongitudeMinute / 60) + (LongitudeSecondDM * 6 / 10 / 3600);       
        }
        public void DecimalChanged()
        {
            Validate(Latitude, 90, out latitude);
            Validate(Longitude, 180, out longitude);
            LatitudeDegree = (int)Latitude;
            LongitudeDegree = (int)Longitude;
            LatitudeMinute = (int)((Latitude - LatitudeDegree) * 100 * 6);
            LongitudeMinute = (int)((Longitude - LongitudeDegree) * 100 * 6);
            LatitudeSecond = (int)((Latitude - LatitudeDegree) - (60 * LatitudeMinute)) * 3600;
            LongitudeSecond = (int)((Longitude - LongitudeDegree) - (60 * LongitudeMinute)) * 3600;
            LatitudeSecondDM = LatitudeSecond / 6 * 10;
            LongitudeSecondDM = LongitudeSecond / 6 * 10;
        }
        public void DMSChanged()
        {
            //if (LongitudeDegree == 90 && (LongitudeMinute>0 || LongitudeSecond>0)) {
            //    longitudeMinute = 0;
            //    longitudeSecond = 0;
            //}
            //Validate(LatitudeDegree, 90, out latitudeDegree);
            //LatitudeDegree = latitudeDegree;

            //Validate(LongitudeDegree, 180, out longitudeDegree);
            //Validate(LongitudeMinute, 60, out longitudeMinute);
            //Validate(LatitudeMinute, 60, out latitudeMinute);
            //Validate(LongitudeSecond, 60, out longitudeSecond);
            //Validate(LatitudeSecond, 60, out latitudeSecond);
            LatitudeSecondDM = LatitudeSecond / 10 * 6;
            LongitudeSecondDM = LongitudeSecond / 10 * 6;
            Latitude = LatitudeDegree + (LatitudeMinute / 60) + (LatitudeSecond / 3600);
            Longitude = LongitudeDegree + (LongitudeMinute / 60) + (LongitudeSecond / 3600);
        }
        public bool Validate(double valueObject, int validateRange, out double result )
        {
            bool validateResult = false;
            double value;
            if (double.TryParse(valueObject.ToString(),out value)) {
                result = valueObject;
                if (0 <= value && value <= validateRange) {
                    validateResult = true;
                }
                else if(value> validateRange) {
                    result = validateRange;
                }else if (value < 0) {
                    result = 0;
                }
            }
            else {
                result = 0;
            }   
            return validateResult;
        }
        public bool Validate(int valueObject, int validateRange, out int result)
        {
            bool validateResult = false;
            double value;
            if (double.TryParse(valueObject.ToString(), out value)) {
                result = valueObject;
                if (0 <= value && value <= validateRange) {
                    validateResult = true;
                }
                else if (value > validateRange) {
                    result = validateRange;
                }
                else if (value < 0) {
                    result = 0;
                }
            }
            else {
                result = 0;
            }
            return validateResult;
        }    
    }
}