﻿using System;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm;
using DevExpress.Mvvm.POCO;

namespace Coordinates_Dialog.ViewModels
{
    [POCOViewModel]
    public class CoordinateViewModel
    {
        public CoordinateViewModel()
        {
            coordinates = new Coordinates();
        }
        public virtual Coordinates coordinates
        {
            get; set;
        }
        public static CoordinateViewModel Create()
        {
            return ViewModelSource.Create(() => new CoordinateViewModel());
        }
        public virtual string test {
            get {
                return "TAT";
            }
            set { }
        }
        public void DMSChanged()
        {
           coordinates.DMSChanged();
        }
        public void DMChanged()
        {
            coordinates.SecondDMChanged();
        }
        public void DecimalChanged()
        {
            coordinates.DecimalChanged();
        }
        public void SaveCoordinates()
        {

        }
    }
}