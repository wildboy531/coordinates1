﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Coordinates_Dialog.Views
{
    /// <summary>
    /// Interaction logic for Coordinate.xaml
    /// </summary>
    public partial class Coordinate : UserControl
    {
        private int LatitudeDegreeLimit = 90;
        private int LatitudeMinuteLimit = 60;
        private int LatitudeSecondLimit = 60;
        private int LatitudeSecondDMLimit = 99;
        private double Latitude = 90;
        private int LongitudeDegreeLimit = 180;
        private int LongitudeMinuteLimit = 60;
        private int LongitudeSecondLimit = 60;
        private int LongitudeSecondDMLimit = 99;
        private double Longitude = 180;
        public Coordinate()
        {
            InitializeComponent();
        }
        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            Window.GetWindow(this).Close();
        }

        private void OKBtn_Click(object sender, RoutedEventArgs e)
        {
            //Data 보내기
            Window.GetWindow(this).Close();
        }

        private void TextEdit_KeyDown(object sender, KeyEventArgs e)
        {
                    }

        private void DMSLatitudeDegree_EditValueChanged(object sender, DevExpress.Xpf.Editors.EditValueChangingEventArgs e)
        {
            int value;
            if (int.TryParse(e.NewValue.ToString(), out value)) {
                if (value > 0 && value <= LatitudeDegreeLimit) {
                    DMSLatitudeDegree.EditValue = e.NewValue;
                }
                else if (value > LatitudeDegreeLimit) {
                    DMSLatitudeDegree.EditValue = LatitudeDegreeLimit;
                }
                else {
                    DMSLatitudeDegree.EditValue = 0;
                }
            }else{
                DMSLatitudeDegree.EditValue = 0;
            }
            DMSLatitudeDegree.Focus();
        }
        public bool Validate(double valueObject, int validateRange, out double result)
        {
            bool validateResult = false;
            double value;
            if (double.TryParse(valueObject.ToString(), out value)) {
                result = valueObject;
                if (0 <= value && value <= validateRange) {
                    validateResult = true;
                }
                else if (value > validateRange) {
                    result = validateRange;
                }
                else if (value < 0) {
                    result = 0;
                }
            }
            else {
                result = 0;
            }
            return validateResult;
        }
        public bool Validate(int valueObject, int validateRange, out int result)
        {
            bool validateResult = false;
            double value;
            if (double.TryParse(valueObject.ToString(), out value)) {
                result = valueObject;
                if (0 <= value && value <= validateRange) {
                    validateResult = true;
                }
                else if (value > validateRange) {
                    result = validateRange;
                }
                else if (value < 0) {
                    result = 0;
                }
            }
            else {
                result = 0;
            }
            return validateResult;
        }
    }
}
}

